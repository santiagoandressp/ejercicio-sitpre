
async function leerJSON(url){
    try{
        let response = await fetch(url);
        let user = await response.json();
        return user;
    } catch(err) {
        alert(err);
    }
}

function leerDatos(){
    const parametro = new URLSearchParams(window.location.search);
    let codigo = parametro.get("codigo");
    let notas  = parametro.get("eliminar");

    let url = "https://programacion-web---i-sem-2019.gitlab.io/persistencia/json_web/json/estudiantes.json";
    leerJSON(url).then(datos => {
        leerEstudiantes(datos.estudiantes, codigo, notas, datos.nombreMateria, datos.descripcion);
    });
}


function leerEstudiantes(estudiantes, codigo, notas, nombreMateria, descripcion){
    //Filtramos el estudiante por su codigo, obtenemos nombre, codigo, arreglo de notas.
    let estudiante = (estudiantes.filter(estudiante => estudiante.codigo == codigo))[0];



    dibujarTablaEst(estudiante, notas, nombreMateria);
    dibujarTablaNot(estudiante, descripcion, notas);
    dibujarPie(estudiante.notas);
}

function validarNotas(notas){

    let array = [];
    let aprobado = 0, desaprobado=0;
    for (let i = 0; i < notas.length; i++) {
        if(notas[i].valor < 3)  aprobado++;
        else desaprobado++;
    }
    array[0] = aprobado;
    array[1] = desaprobado;
    return array;
}

function dibujarTablaEst(estudiante, notas, nombreMateria){

    var data = new google.visualization.DataTable();
    data.addColumn("string");
    data.addRows([
        [`Nombre:${estudiante.nombre}`],
        [`Código:${estudiante.codigo}`],
        [`Materia:${nombreMateria}`],
    ]);

    var table = new google.visualization.Table(document.getElementById('table_div'));
    table.draw(data, {showRowNumber: false, width: '100%', height: '100%'});
}

function dibujarTablaNot(estudiante,descripciones, maxNotas){
// que tan malo es pasar el json a un arreglo de objeto para luego 
//  manejarlo mas comodamente

    let notas = estudiante.notas;

    var data = new google.visualization.DataTable();
    data.addColumn("string","Descripcion");
    data.addColumn("number", "Valor");
    data.addColumn("string", "Observacion");
    data.addRows(notas.length+1);

    let observacion = calcularMensajes(notas,maxNotas);

    for (let i = 0; i < notas.length; i++) {
        data.setCell(i,0,descripciones[(notas[i].id)-1].descripcion);
        data.setCell(i,1,notas[i].valor);
        data.setCell(i,2,observacion[i]);
    }

    let notasProm = [...notas];
    notasProm.splice((notas.length-maxNotas),notas.length)

    let prome = promedio(notasProm); 

    data.setCell(notas.length,0,"NOTA DEFINITIVA");
    data.setCell(notas.length,1,prome.toFixed(2));
    data.setCell(notas.length,2,"NOTA DEFINITIVA");


    var table = new google.visualization.Table(document.getElementById('tableNotas_div'));
    table.draw(data, {showRowNumber: false, width: '100%', height: '100%'});
}

function dibujarPie(notas) {
    let array = validarNotas(notas);
    var data = google.visualization.arrayToDataTable([
        ["Estado","Porcentaje"],
        ["Aprobado",array[0]],
        ["Desaprobado",array[1]]
    ]);
    var options = {
        title: '',
        is3D: true,
    };
    var chart = new google.visualization.PieChart(document.getElementById('pie_div'));
    chart.draw(data, options);
}

// function calcularMensajes(notas,maxNotas) {

//     let msg = [];
//     let array = [];
//     let notass = [...notas];

//     notass = notass.map((nota, index) => nota = notas[index].valor);
//     notass.sort((a, b) => a - b);

//     for (let i = 0; i < maxNotas ; i++) {
//         array.push(notas[i]);
//     }
    
//     return array
//}

function calcularMensajes(notass,maxNotas){
    let msg = [];
    notass.sort((a, b) => b.valor - a.valor);
    console.log(notass);
    for (let i = 0; i < notass.length; i++){
        
            if(notass[i].valor >= 3){
                msg[i] = "Nota Aprobada";
            }
            if(notass[i].valor < 3){
                msg[i] = "Nota Desaprobada";
            }

            if(i>=notass.length-maxNotas){
                msg[i] = "Nota Eliminada";
            }

    }

    return msg;
}


let promedio = (notas=>{
    let prom = 0;
    notas.forEach(nota=> {
        prom+= nota.valor;
    });
    return prom/notas.length
})